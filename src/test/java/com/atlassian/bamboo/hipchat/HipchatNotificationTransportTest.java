package com.atlassian.bamboo.hipchat;

import com.atlassian.bamboo.build.LogEntry;
import com.atlassian.bamboo.event.BuildHungEvent;
import com.atlassian.bamboo.notification.buildhung.BuildHungNotification;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.project.Project;
import com.google.common.collect.Lists;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.lang.reflect.Field;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class HipchatNotificationTransportTest
{
    private final String API_TOKEN = "alamakotaakotmaapitoken";
    private final String ROOM = "theRoom";
    private final boolean NOTIFY_USERS = true;

    @Rule
    public final RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    @Mock
    private Project project;
    @Mock
    private Plan plan;

    @Test
    public void testCorrectUrlsAreHit()
    {
        when(project.getKey()).thenReturn("BAM");
        when(plan.getProject()).thenReturn(project);
        when(plan.getBuildKey()).thenReturn("MAIN");
        when(plan.getName()).thenReturn("Main");

        final PlanResultKey planResultKey = PlanKeys.getPlanResultKey("BAM-MAIN", 3);
        final BuildHungEvent event = new BuildHungEvent(this, planResultKey.getPlanKey().getKey(), planResultKey.getBuildNumber(), null, Lists.<LogEntry>newArrayList());

        BuildHungNotification notification = new BuildHungNotification()
        {
            public String getHtmlImContent()
            {
                return "IM Content";
            }

            public BuildHungEvent getEvent()
            {
                return event;
            }
        };

        HipchatNotificationTransport hnt = new HipchatNotificationTransport(API_TOKEN, ROOM, NOTIFY_USERS, null, null);

        //dirty reflections trick to inject mock HttpClient
        try
        {
            Field field = HipchatNotificationTransport.class.getDeclaredField("client");
            field.setAccessible(true);
            field.set(hnt, new MockHttpClient());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail(e.getMessage());
        }

        hnt.sendNotification(notification);
    }

    @Test
    public void determineHipChatApiBaseUrlReturnsDefaultValue() throws Exception
    {
        assertThat(HipchatNotificationTransport.determineHipChatApiBaseUrl(), equalTo("https://api.hipchat.com"));
    }

    @Test
    public void determineHipChatApiBaseUrlReturnsCustomValue() throws Exception
    {
        final String customHipChatApiUrl = "https://hipchat.mycompany.com";
        System.setProperty("hipchat.api.url", customHipChatApiUrl);

        assertThat(HipchatNotificationTransport.determineHipChatApiBaseUrl(), equalTo(customHipChatApiUrl));
    }

    public class MockHttpClient extends org.apache.commons.httpclient.HttpClient
    {
        public int executeMethod(HttpMethod method)
                throws IOException, HttpException
        {
            assertTrue(method instanceof PostMethod);
            PostMethod postMethod = (PostMethod) method;
            assertEquals(HipchatNotificationTransport.HIPCHAT_API_URL + API_TOKEN, method.getURI().toString());
            assertEquals(ROOM, postMethod.getParameter("room_id").getValue());
            assertEquals("Bamboo", postMethod.getParameter("from").getValue());
            assertEquals("1", postMethod.getParameter("notify").getValue());
            return 0;
        }
    }
}
