package com.atlassian.bamboo.hipchat;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class HipchatNotificationRecipientExporterTest
{
    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks private HipchatNotificationRecipientExporter exporter;

    @Test
    public void exporterPublishRequiredFields()
    {
        final Map<String, Object> map = exporter.toSerializableMap("123|true|room");

        assertThat(((String) map.get(HipchatNotificationRecipient.API_TOKEN)), is("123"));
        assertThat(((String) map.get(HipchatNotificationRecipient.ROOM)), is("room"));
        assertThat(((Boolean) map.get(HipchatNotificationRecipient.NOTIFY_USERS)), is(true));
    }
}