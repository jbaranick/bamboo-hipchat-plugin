package com.atlassian.bamboo.hipchat;

import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.atlassian.bamboo.variable.substitutor.VariableSubstitutor;
import com.atlassian.bamboo.variable.substitutor.VariableSubstitutorFactory;
import com.google.common.collect.ImmutableMap;
import org.jetbrains.annotations.Nullable;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;

import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class HipchatNotificationRecipientTest
{
    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock private CustomVariableContext customVariableContext;
    @Mock private VariableSubstitutorFactory variableSubstitutorFactory;

    @InjectMocks private HipchatNotificationRecipient hipchatNotificationRecipient;

    @Before
    public void setUp()
    {
        doReturn(variableSubstitutorFactory).when(customVariableContext).getVariableSubstitutorFactory();
    }

    /**
     * Test variable substitution for Hipchat notifications when build plan is known.
     */
    @Test
    public void testVariableSubstitutionWithPlan() throws Exception
    {
        final ImmutablePlan plan = mock(ImmutablePlan.class);
        testVariableSubstitution(plan);
        verify(variableSubstitutorFactory).newSubstitutorForPlan(plan);
    }

    /**
     * Test variable substitution for Hipchat notifications when build plan is unknown.
     */
    @Test
    public void testVariableSubstitutionWithoutPlan() throws Exception
    {
        testVariableSubstitution(null);
        verify(variableSubstitutorFactory).newSubstitutorForGlobalContext();
    }

    private void testVariableSubstitution(@Nullable ImmutablePlan plan)
    {
        final String apiTokenVariable = "${bamboo.apiToken}";
        final String apiToken = "2df72984171f860430bc90ce0722f8";
        final String roomVariable = "${bamboo.room}";
        final String room = "Bamboo";

        doReturn(apiToken).when(customVariableContext).substituteString(apiTokenVariable);
        doReturn(room).when(customVariableContext).substituteString(roomVariable);
        doAnswer(runWithVariableSubstitutor()).when(customVariableContext).withVariableSubstitutor(any(VariableSubstitutor.class), any(Runnable.class));

        final Map<String, String[]> configuration =
                ImmutableMap.<String, String[]>builder()
                        .put(HipchatNotificationRecipient.API_TOKEN, new String[]{apiTokenVariable})
                        .put(HipchatNotificationRecipient.ROOM, new String[]{roomVariable})
                        .put(HipchatNotificationRecipient.NOTIFY_USERS, new String[]{})
                        .build();

        hipchatNotificationRecipient.populate(configuration);
        hipchatNotificationRecipient.setPlan(plan);

        final List<NotificationTransport> transports = hipchatNotificationRecipient.getTransports();

        assertThat(transports, hasSize(1));
        assertThat(transports.get(0), instanceOf(HipchatNotificationTransport.class));
        final HipchatNotificationTransport notificationTransport = (HipchatNotificationTransport) transports.get(0);

        assertThat(notificationTransport.apiToken, is(apiToken));
        assertThat(notificationTransport.room, is(room));
    }

    private Answer<Void> runWithVariableSubstitutor()
    {
        return new Answer<Void>()
        {
            @Override
            public Void answer(InvocationOnMock invocationOnMock) throws Throwable
            {
                final Runnable runnable = (Runnable) invocationOnMock.getArguments()[1];
                runnable.run();
                return null;
            }
        };
    }
}
