package com.atlassian.bamboo.hipchat;

import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.builder.LifeCycleState;
import com.atlassian.bamboo.deployments.results.DeploymentResult;
import com.atlassian.bamboo.notification.Notification;
import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.util.UrlUtils;
import com.atlassian.bamboo.utils.HttpUtils;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class HipchatNotificationTransport implements NotificationTransport
{
    private static final Logger log = Logger.getLogger(HipchatNotificationTransport.class);

    private static final String HIPCHAT_API_BASE_PROPERTY = "hipchat.api.url";
    private static final String HIPCHAT_API_BASE_DEFAULT = "https://api.hipchat.com";

    private static final String HIPCHAT_API_BASE = determineHipChatApiBaseUrl();
    public static final String HIPCHAT_API_URL = HIPCHAT_API_BASE + "/v1/rooms/message?auth_token=";

    //colors, available options (as of 18.06.2013) are: yellow, red, green, purple and grey
    public static final String COLOR_UNKNOWN_STATE = "gray";
    public static final String COLOR_FAILED = "red";
    public static final String COLOR_SUCCESSFUL = "green";
    public static final String COLOR_IN_PROGRESS = "yellow";

    @VisibleForTesting final String apiToken;
    @VisibleForTesting final String room;
    private final static String from = "Bamboo";
    private final boolean notify;

    private final HttpClient client;

    @Nullable
    private final ResultsSummary resultsSummary;
    @Nullable
    private final DeploymentResult deploymentResult;

    public HipchatNotificationTransport(String apiToken,
                                        String room,
                                        boolean notify,
                                        @Nullable ResultsSummary resultsSummary,
                                        @Nullable DeploymentResult deploymentResult)
    {
        this.apiToken = apiToken;
        this.room = room;
        this.notify = notify;
        this.resultsSummary = resultsSummary;
        this.deploymentResult = deploymentResult;
        client = new HttpClient();

        try
        {
            URI uri = new URI(HIPCHAT_API_URL);
            setProxy(client, uri.getScheme(), uri.getHost());
        }
        catch (URIException e)
        {
            log.error("Unable to set up proxy settings, invalid URI encountered: " + e);
        }
        catch (URISyntaxException e)
        {
            log.error("Unable to set up proxy settings, invalid URI encountered: " + e);
        }
    }

    @Override
    public void sendNotification(Notification notification)
    {

        String message = (notification instanceof Notification.HtmlImContentProvidingNotification)
                ? ((Notification.HtmlImContentProvidingNotification) notification).getHtmlImContent()
                : notification.getIMContent();

        if (!StringUtils.isEmpty(message))
        {
            PostMethod method = setupPostMethod();
            method.setParameter("message",message);
            if (resultsSummary != null)
            {
                setMessageColor(method, resultsSummary);
            }
            else if (deploymentResult != null)
            {
                setMessageColor(method, deploymentResult);
            }
            else
            {
                setMessageColor(method, COLOR_UNKNOWN_STATE); //todo: might need to use different color in some cases
            }

            try
            {
                client.executeMethod(method);
            }
            catch (IOException e)
            {
                log.error("Error using Hipchat API: " + e.getMessage(), e);
            }
        }
    }

    private void setMessageColor(PostMethod method, ResultsSummary result)
    {
        String color = COLOR_UNKNOWN_STATE;

        if (result.getBuildState() == BuildState.FAILED)
        {
            color = COLOR_FAILED;
        }
        else if (result.getBuildState() == BuildState.SUCCESS)
        {
            color = COLOR_SUCCESSFUL;
        }
        else if (LifeCycleState.isActive(result.getLifeCycleState()))
        {
            color = COLOR_IN_PROGRESS;
        }

        setMessageColor(method, color);
    }

    private void setMessageColor(PostMethod method, DeploymentResult deploymentResult)
    {
        String color = COLOR_UNKNOWN_STATE;

        if (deploymentResult.getDeploymentState() == BuildState.FAILED)
        {
            color = COLOR_FAILED;
        }
        else if (deploymentResult.getDeploymentState() == BuildState.SUCCESS)
        {
            color = COLOR_SUCCESSFUL;
        }
        else if (LifeCycleState.isActive(deploymentResult.getLifeCycleState()))
        {
            color = COLOR_IN_PROGRESS;
        }

        setMessageColor(method, color);
    }

    private void setMessageColor(PostMethod method, String colour)
    {
        method.addParameter("color", colour);
    }

    private PostMethod setupPostMethod()
    {
        final PostMethod m = new PostMethod(HIPCHAT_API_URL + apiToken);
        m.setRequestHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED + "; charset=UTF-8");
        m.addParameter("room_id", room);
        m.addParameter("from", from);
        m.addParameter("notify", (notify ? "1" : "0"));
        return m;
    }

    private static void setProxy(@NotNull final HttpClient client, @Nullable final String scheme, @NotNull final String hostname) throws URIException
    {
        HttpUtils.EndpointSpec proxyForScheme = HttpUtils.getProxyForScheme(scheme);
        if (proxyForScheme!=null && !isNonProxyHost(hostname))
        {
            client.getHostConfiguration().setProxy(proxyForScheme.host, proxyForScheme.port);
        }
    }

    private static boolean isNonProxyHost(String host)
    {
        final String httpNonProxyHosts = System.getProperty("http.nonProxyHosts");
        if (StringUtils.isBlank(httpNonProxyHosts))
        {
            // checking if property was misspelt, notice there is no 's' at the end of this property
            if (StringUtils.isNotBlank(System.getProperty("http.nonProxyHost")))
            {
                log.warn("The system property http.nonProxyHost is set. You probably meant to set http.nonProxyHosts.");
            }
            return false;
        }
        final String[] nonProxyHosts = httpNonProxyHosts.split("\\|");
        for (String nonProxyHost : nonProxyHosts)
        {
            if (nonProxyHost.startsWith("*"))
            {
                if (host.endsWith(nonProxyHost.substring(1)))
                {
                    return true;
                }
            }
            else if (host.equals(nonProxyHost))
            {
                return true;
            }
        }
        return false;
    }

    @VisibleForTesting
    static String determineHipChatApiBaseUrl()
    {
        final String apiBase = UrlUtils.stripTailingSlashes(System.getProperty(HIPCHAT_API_BASE_PROPERTY, HIPCHAT_API_BASE_DEFAULT));

        log.info(apiBase + " will be used for HipChat integration (if enabled).");
        return apiBase;
    }
}
