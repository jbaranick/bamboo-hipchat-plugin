package com.atlassian.bamboo.hipchat;

import com.atlassian.bamboo.notification.NotificationRecipientExporter;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedHashMap;
import java.util.Map;

public class HipchatNotificationRecipientExporter implements NotificationRecipientExporter
{
    private static final Logger log = Logger.getLogger(HipchatNotificationRecipientExporter.class);

    @NotNull
    @Override
    public Map<String, Object> toSerializableMap(@Nullable String recipient)
    {
        Map<String, Object> result = new LinkedHashMap<String, Object>();
        try
        {
            HipchatRecipientConfiguration config = HipchatRecipientConfiguration.parse(recipient);
            result.put(HipchatNotificationRecipient.API_TOKEN, config.getApiToken());
            result.put(HipchatNotificationRecipient.ROOM, config.getRoom());
            result.put(HipchatNotificationRecipient.NOTIFY_USERS, config.isNotify());
        }
        catch (Exception e)
        {
            log.error(e.getMessage(), e);
        }
        return result;
    }
}
