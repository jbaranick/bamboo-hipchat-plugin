package com.atlassian.bamboo.hipchat;

import com.atlassian.bamboo.deployments.notification.DeploymentResultAwareNotificationRecipient;
import com.atlassian.bamboo.deployments.results.DeploymentResult;
import com.atlassian.bamboo.notification.NotificationRecipient;
import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.notification.recipients.AbstractNotificationRecipient;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plugin.descriptor.NotificationRecipientModuleDescriptor;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.template.TemplateRenderer;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.atlassian.bamboo.variable.substitutor.VariableSubstitutor;
import com.atlassian.bamboo.variable.substitutor.VariableSubstitutorFactory;
import com.atlassian.event.Event;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;

public class HipchatNotificationRecipient extends AbstractNotificationRecipient implements DeploymentResultAwareNotificationRecipient,
                                                                                           NotificationRecipient.RequiresPlan,
                                                                                           NotificationRecipient.RequiresResultSummary,
                                                                                           NotificationRecipient.RequiresEvent

{
    private static final Logger log = Logger.getLogger(HipchatNotificationRecipient.class);

    @VisibleForTesting static String API_TOKEN = "apiToken";
    @VisibleForTesting static String ROOM = "room";
    @VisibleForTesting static String NOTIFY_USERS = "notifyUsers";

    private String apiToken = null;
    private String room = null;
    private boolean notify = false;

    private TemplateRenderer templateRenderer;

    private ImmutablePlan plan;
    private ResultsSummary resultsSummary;
    private DeploymentResult deploymentResult;
    private Event event;
    private CustomVariableContext customVariableContext;

    @Override
    public void populate(@NotNull Map<String, String[]> params)
    {
        for (String next : params.keySet())
        {
            System.out.println("next = " + next);
        }
        if (params.containsKey(API_TOKEN))
        {
            this.apiToken = params.get(API_TOKEN)[0];
        }
        if (params.containsKey(ROOM))
        {
            this.room = params.get(ROOM)[0];
        }

        this.notify = params.containsKey(NOTIFY_USERS);
    }

    @Override
    public void init(@Nullable String configurationData)
    {
        if (configurationData != null)
        {
            try
            {
                parseConfiguration(configurationData);
            }
            catch (Exception e)
            {
                log.info("Can't parse configuration data", e);
            }
        }
    }

    private void parseConfiguration(@Nullable String configurationData)
    {
        HipchatRecipientConfiguration config = HipchatRecipientConfiguration.parse(configurationData);
        this.apiToken = config.getApiToken();
        this.room = config.getRoom();
        this.notify = config.isNotify();
    }

    @NotNull
    @Override
    public String getRecipientConfig()
    {
        // We can do this because API tokens don't have | in them, but it's pretty dodge. Better to JSONify or something?
        return apiToken + '|' + String.valueOf(notify) + '|' + room;
    }

    @NotNull
    @Override
    public String getEditHtml()
    {
        String editTemplateLocation = ((NotificationRecipientModuleDescriptor)getModuleDescriptor()).getEditTemplate();
        return templateRenderer.render(editTemplateLocation, populateContext());
    }

    private Map<String, Object> populateContext()
    {
        Map<String, Object> context = Maps.newHashMap();
        if (apiToken != null)
        {
            context.put(API_TOKEN, apiToken);
        }
        if (room != null)
        {
            context.put(ROOM, room);
        }
        context.put(NOTIFY_USERS, notify);
        return context;
    }

    @NotNull
    @Override
    public String getViewHtml()
    {
        String editTemplateLocation = ((NotificationRecipientModuleDescriptor)getModuleDescriptor()).getViewTemplate();
        return templateRenderer.render(editTemplateLocation, populateContext());
    }

    @NotNull
    @Override
    public List<NotificationTransport> getTransports()
    {
        final List<NotificationTransport> list = Lists.newArrayList();

        final VariableSubstitutorFactory variableSubstitutorFactory = customVariableContext.getVariableSubstitutorFactory();
        final VariableSubstitutor variableSubstitutor = plan != null
                ? variableSubstitutorFactory.newSubstitutorForPlan(plan)
                : variableSubstitutorFactory.newSubstitutorForGlobalContext();

        customVariableContext.withVariableSubstitutor(variableSubstitutor, new Runnable()
        {
            @Override
            public void run()
            {
                list.add(
                        new HipchatNotificationTransport(
                                customVariableContext.substituteString(apiToken),
                                customVariableContext.substituteString(room),
                                notify, resultsSummary, deploymentResult));
            }
        });

        return list;
    }

    @Override
    public void setEvent(@Nullable final Event event)
    {
        this.event = event;
    }

    public void setPlan(@Nullable final Plan plan)
    {
        this.plan = plan;
    }

    @Override
    public void setPlan(@Nullable final ImmutablePlan plan)
    {
        this.plan = plan;
    }

    @Override
    public void setDeploymentResult(@Nullable final DeploymentResult deploymentResult)
    {
        this.deploymentResult = deploymentResult;
    }

    @Override
    public void setResultsSummary(@Nullable final ResultsSummary resultsSummary)
    {
        this.resultsSummary = resultsSummary;
    }

    public void setTemplateRenderer(TemplateRenderer templateRenderer)
    {
        this.templateRenderer = templateRenderer;
    }

    public void setCustomVariableContext(CustomVariableContext customVariableContext) { this.customVariableContext = customVariableContext; }
}
