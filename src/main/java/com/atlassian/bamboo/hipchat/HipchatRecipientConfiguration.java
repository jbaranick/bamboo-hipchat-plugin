package com.atlassian.bamboo.hipchat;

class HipchatRecipientConfiguration
{
    private final String apiToken;
    private final String room;
    private final boolean notify;

    private HipchatRecipientConfiguration(String apiToken, String room, boolean notify)
    {
        this.apiToken = apiToken;
        this.room = room;
        this.notify = notify;
    }

    public static HipchatRecipientConfiguration parse(final String configuration)
    {
        int firstIdx = configuration.indexOf('|');
        if (firstIdx > 0)
        {
            int secondIdx = configuration.indexOf('|', firstIdx + 1);
            String apiToken = configuration.substring(0, firstIdx);
            String room = configuration.substring(secondIdx + 1);
            boolean notify = configuration.substring(firstIdx + 1, secondIdx).equals("true");
            return new HipchatRecipientConfiguration(apiToken, room, notify);
        }

        throw new IllegalArgumentException("Can't parse provided string:" + configuration);
    }

    public String getApiToken()
    {
        return apiToken;
    }

    public String getRoom()
    {
        return room;
    }

    public boolean isNotify()
    {
        return notify;
    }
}
